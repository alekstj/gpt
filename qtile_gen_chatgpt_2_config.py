# Import necessary libraries
from typing import List  # noqa: F401

# Import the basic libqtile modules
from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy

# Define some variables
mod = "mod4"  # Sets mod key to Super or Windows key
terminal = "alacritty"  # Sets default terminal

# Define key bindings
keys = [
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "j", lazy.layout.down()),

    # Move windows up or down in current stack
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),

    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next()),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate()),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout()),
    Key([mod, "shift"], "q", lazy.window.kill()),

    # Restart qtile
    Key([mod, "control"], "r", lazy.restart()),

    # Shutdown qtile
    Key([mod, "control"], "q", lazy.shutdown()),

    # Spawn terminal
    Key([mod], "Return", lazy.spawn(terminal)),

    # Toggle fullscreen for focused window
    Key([mod], "f", lazy.window.toggle_fullscreen()),

    # Toggle floating mode for focused window
    Key([mod, "shift"], "f", lazy.window.toggle_floating()),

    # Toggle between different screens
    Key([mod], "h", lazy.to_screen(0)),
    Key([mod], "l", lazy.to_screen(1)),
]

# Define groups
group_names = [("1", {"layout": "columns"}),
               ("2", {"layout": "max"}),
               ("3", {"layout": "stack"}),
               ("4", {"layout": "bsp"}),
               ("5", {"layout": "monadtall"}),
               ("6", {"layout": "floating"}),
               ("7", {"layout": "matrix"}),
               ("8", {"layout": "treetab"}),
               ("9", {"layout": "ratio"}),
               ]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))  # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name)))  # Send current window to another group

# Define layouts
layouts = [
    layout.Columns(border_focus_stack='#d75f5f'),
    layout.Max(),
    layout.Stack(num_stacks=2),
    layout.Bsp(),
    layout.MonadTall(),
    layout.Floating(),
    layout.Matrix(),
    layout.TreeTab(),
    layout.RatioTile(),
]

# Define widget settings
widget_defaults = dict(
    font='sans',
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

# Define bar
screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords=[{mod}, "x"],
                    name="quit",
                    on_press=lambda: qtile.cmd_spawn("shutdown")
                ),
                widget.Clock(format='%Y-%m-%d %a %I:%M %p'),
                widget.QuickExit(),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

# Add custom hooks
def autostart():
    # Start some applications when Qtile starts
    lazy.spawn("nm-applet")  # Network manager applet
    lazy.spawn("volumeicon")  # Volume icon
    lazy.spawn("flameshot")  # Screenshot tool

# Call autostart function
autostart()
