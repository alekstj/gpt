# Import necessary modules
from typing import List  # noqa: F401

# Import Qtile modules
from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

# Define mod key (Super key)
mod = "mod4"
terminal = guess_terminal

# Define key bindings
keys = [
    # Terminal
    Key([mod], "Return", lazy.spawn(terminal)),

    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down()),
    Key([mod], "j", lazy.layout.up()),

    # Move windows up or down in current stack
    Key([mod, "shift"], "k", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_up()),

    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next()),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate()),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout()),

    # Kill focused window
    Key([mod], "q", lazy.window.kill()),

    # Restart Qtile
    Key([mod, "control"], "r", lazy.restart()),

    # Shutdown Qtile
    Key([mod, "control"], "q", lazy.shutdown()),
]

# Define groups
group_names = [("1", {"layout": "monadtall"}),
               ("2", {"layout": "monadtall"}),
               ("3", {"layout": "monadtall"}),
               ("4", {"layout": "monadtall"}),
               ("5", {"layout": "monadtall"}),
               ("6", {"layout": "monadtall"}),
               ("7", {"layout": "monadtall"}),
               ("8", {"layout": "monadtall"}),
               ("9", {"layout": "monadtall"})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

# Define layouts
layouts = [
    layout.MonadTall(border_width=1, margin=4),
    layout.Max(),
]

# Define widget bar
widget_defaults = dict(
    font="sans",
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(),
                widget.Prompt(),
                widget.WindowName(),
                widget.TextBox("default config", name="default"),
                widget.Systray(),
                widget.Clock(format='%Y-%m-%d %a %I:%M %p'),
            ],
            24,  # Adjust this value if you want to change the bar height
        ),
    ),
]

# Define mouse bindings
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

# Define floating layout configuration
floating_layout = layout.Floating(float_rules=[
    {"wmclass": "confirm"},
    {"wmclass": "dialog"},
    {"wmclass": "download"},
    {"wmclass": "error"},
    {"wmclass": "file_progress"},
    {"wmclass": "notification"},
    {"wmclass": "splash"},
    {"wmclass": "toolbar"},
    {"wmclass": "confirmreset"},  # firefox reset popup
    {"wmclass": "makebranch"},  # firefox extension install dialog
    {"wmclass": "maketageditor"},  # firefox extension install dialog
    {"wname": "branchdialog"},  # firefox extension install dialog
    {"wname": "saveas"},
    {"wname": "Save As"},
    {"wname": "open"},
    {"wname": "Open"},
    {"wname": "filechooserdialog"},  # gtk file dialog
])

# Define auto start applications
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# Define the main function
def main(qtile):
    pass
